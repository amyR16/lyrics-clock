function lyrics(){
    console.log("lyrics");
    var imgs = [
        "images/1.jpg",
        "images/2.jpg",
        "images/3.jpg",
        "images/4.jpg",
        "images/5.jpg",
        "images/6.jpg",
        "images/7.jpg",
        "images/8.jpg",
        "images/9.jpg",
        "images/10.jpg",
        "images/11.jpg",
        "images/12.jpg",
        "images/13.jpg",
        "images/14.jpg",
        "images/15.jpg",
        "images/16.jpg",
        "images/17.jpg",
        "images/18.jpg",
        "images/19.jpg",
        "images/20.jpg",
        "images/21.jpg",
        "images/22.jpg",
        "images/23.jpg",
        "images/24.jpg"
    ],
    len = imgs.length,
    idx = -1;

 setInterval(function(){

    var hours = new Date().getHours();
    $('#hours').html((hours < 24 ? "0" : "") + hours);
    idx = (idx+1)%len;

    if (hours == 0) {
        idx = 0;
    } else if (hours == 1){
        idx = 1;
    }else if (hours == 2){
        idx = 2;
        console.log("2am");
    }else if (hours == 3){
        idx = 3;
    }else if (hours == 4){
        idx = 4;
    }else if (hours == 5){
        idx = 5;
    }else if (hours == 6){
        idx = 6;
    }else if (hours == 7){
        idx = 7;
    }else if (hours == 8){
        idx = 8;
    }else if (hours == 9){
        idx = 9;
    }else if (hours == 10){
        idx = 10;
    }else if (hours == 11){
        idx = 11;
    }else if (hours == 12){
        idx = 12;
    }else if (hours == 13){
        idx = 13;
    }else if (hours == 14){
        idx = 14;
    }else if (hours == 15){
        idx = 15;
    }else if (hours == 16){
        idx = 16;
    }else if (hours == 17){
        idx = 17;
    }else if (hours == 18){
        idx = 18;
    }else if (hours == 19){
        idx = 19;
    }else if (hours == 20){
        idx = 20;
    }else if (hours == 21){
        idx = 21;
    }else if (hours == 22){
        idx = 22;
    }else if (hours == 23){
        idx = 23;
    }else if (hours == 24){
        idx = 24;
    }

    $("body").css("background", "#000 url("+imgs[idx]+")");
}, 1000); 
}

lyrics();